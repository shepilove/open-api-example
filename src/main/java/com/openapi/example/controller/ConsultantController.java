package com.openapi.example.controller;

import com.openapi.example.api.ConsultantApi;
import com.openapi.example.model.ConsultantDto;
import com.openapi.example.services.ConsultantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/consultants")
@RequiredArgsConstructor
public class ConsultantController implements ConsultantApi {

    private final ConsultantService service;

    @Override
    public ResponseEntity<ConsultantDto> create(ConsultantDto consultantDto) {
        return ResponseEntity.ok(service.create(consultantDto));
    }

    @Override
    public ResponseEntity<ConsultantDto> get(Long id) {
        return ResponseEntity.ok(service.get(id));
    }

    @Override
    public ResponseEntity<ConsultantDto> update(Long id, ConsultantDto consultantDto) {
        service.update(id, consultantDto);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<ConsultantDto>> getAllConsultants() {
        return ResponseEntity.ok(service.getAll());
    }
}

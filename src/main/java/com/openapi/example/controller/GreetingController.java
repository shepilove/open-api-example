package com.openapi.example.controller;

import com.openapi.example.api.TestApi;
import com.openapi.example.services.InfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/test")
@RequiredArgsConstructor
public class GreetingController implements TestApi {

    private final InfoService greeter;

    @Override
    public ResponseEntity<String> test() {
        return ResponseEntity.ok(greeter.getGreet());
    }
}

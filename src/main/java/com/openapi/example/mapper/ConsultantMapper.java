package com.openapi.example.mapper;

import com.openapi.example.model.Consultant;
import com.openapi.example.model.ConsultantDto;
import org.mapstruct.Mapper;

@Mapper
public interface ConsultantMapper {

    Consultant map(ConsultantDto consultantDto);

    ConsultantDto mapToDto(Consultant consultant);
}

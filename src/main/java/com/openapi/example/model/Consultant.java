package com.openapi.example.model;

import javax.persistence.*;

import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Consultant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private int age;
    private Boolean isAssigned;
    private String client;

    @Override
    public String toString() {
        return String.format("Consultant Id: %s%nFirst Name: %s%nLast Name: %s%nAge: %d%nAssigned: %s%nClient: %s%n%n",
                id, firstName, lastName, age, isAssigned, isAssigned ? client : "N/A");
    }
}

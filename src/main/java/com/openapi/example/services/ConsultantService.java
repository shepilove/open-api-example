package com.openapi.example.services;

import java.util.List;

import com.openapi.example.model.ConsultantDto;
import org.springframework.stereotype.Service;

@Service
public interface ConsultantService {

    List<ConsultantDto> getAll();

    ConsultantDto create(ConsultantDto newConsultant);

    ConsultantDto get(Long id);

    void update(Long id, ConsultantDto consultant);

    void delete(Long id);
}

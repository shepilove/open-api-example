package com.openapi.example.services.impl;

import java.util.List;
import java.util.Random;

import com.openapi.example.mapper.ConsultantMapper;
import com.openapi.example.model.Consultant;
import com.openapi.example.model.ConsultantDto;
import com.openapi.example.services.ConsultantService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConsultantServiceImpl implements ConsultantService {

    private final ConsultantMapper mapper;

    @Override
    public List<ConsultantDto> getAll() {
        return List.of(mapper.mapToDto(new Consultant(1L, "firstName", "lastName", 25, true, "Some Client")));
    }

    @Override
    public ConsultantDto create(ConsultantDto newConsultant) {
        newConsultant.setId(new Random().nextLong());
        return newConsultant;
    }

    @Override
    public ConsultantDto get(Long id) {
        return mapper.mapToDto(new Consultant(id, "firstName", "lastName", 25, true, "Some Client"));
    }

    @Override
    public void update(Long id, ConsultantDto consultant) {
        System.out.println("Consultant with id " + id + " has been updated");
    }

    @Override
    public void delete(Long id) {
        System.out.println("Consultant with id " + id + " has been deleted");
    }
}

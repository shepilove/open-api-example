package com.openapi.example.services.impl;

import com.openapi.example.services.InfoService;
import org.springframework.stereotype.Service;

@Service
public class InfoServiceImpl implements InfoService {

    @Override
    public String getGreet() {
        return "Consultant-Api version 1 is up and running.";
    }
}
